# NOTE

## BUG
* NullPointerException: ketika Customer (Member) membuat booking, kemudian kembali membuat booking namun pada saat konfirmasi pembayaran dibatalkan. Ketika mencoba menampilkan Booking Information, aplikasi crash karena NullPointerException. Sedangkan untuk customer (non member) tidak terjadi crash.
* Data booking order (di simpan di ArrayList) untuk Customer (member) tidak tersimpan ketika customer membuat booking, lalu customer logout dan login kembali data booking tidak tersimpan. Sedangkan untuk customer (non member) tersimpan.