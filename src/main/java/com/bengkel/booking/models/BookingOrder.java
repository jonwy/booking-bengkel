package com.bengkel.booking.models;

import java.lang.reflect.Member;
import java.util.Date;
import java.util.List;
import com.bengkel.booking.interfaces.IBengkelPayment;
import com.bengkel.booking.util.IDGenerator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookingOrder implements IBengkelPayment{
	private static int indexCreation = 1;
	public static final int ORDER_PAYMENT_FINALIZED = 1;
	public static final int ORDER_PAYMENT_CANCELED = -1;


	private String bookingId;
	private Customer customer;
	private List<ItemService> services;
	private String paymentMethod;
	private double totalServicePrice;
	private double totalPayment;
	private int status;
	
	public BookingOrder(Customer customer) {
		this.customer = customer;
		bookingId = IDGenerator.generateId("Book-" + customer.getCustomerId(), indexCreation++);
	}

	@Override
	public void calculatePayment() {
		double discount = 0;
		if (paymentMethod.equalsIgnoreCase("Saldo Coin")) {
			System.out.println("Customer is using coin");
			discount = getTotalServicePrice() * RATES_DISCOUNT_SALDO_COIN;
		}else {
			discount = getTotalServicePrice() * RATES_DISCOUNT_CASH;
		}
		setTotalPayment(getTotalServicePrice() - discount);
	}

	
}
