package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static Map<Customer, List<BookingOrder>> bookingOrderMap = new HashMap<>();
	private static Scanner input = new Scanner(System.in);
	public static void run() {
		boolean isLooping = true;
		do {
			Customer customer = login();
			if (customer != null) {
				if (bookingOrderMap.isEmpty() || !bookingOrderMap.containsKey(customer)) {
					bookingOrderMap.put(customer, new ArrayList<>());
				}
				mainMenu(customer, bookingOrderMap.get(customer));
			}
		} while (isLooping);
		
	}
	
	public static Customer login() {
		System.out.println("Aplikasi Booking Bengkel");
		System.out.println("1. Login");
		System.out.println("0. Back");
		int choice = Validation.validasiNumberWithRange("Choice: ", "Input hanya angka", "[0-9]", 1, 0);
		switch (choice) {
			case 1:
				return BengkelService.login(listAllCustomers);
			case 0:
				System.out.println("Keluar dari program");
				System.exit(0);
		}
		return null;
	}
	
	public static void mainMenu(Customer customer, List<BookingOrder> bookingOrders) {
		System.out.println("Aplikasi Booking Bengkel\n");
		System.out.println("Selamat datang, " + customer.getName() + "!");

		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				BengkelService.showCustomerInfo(customer);
				break;
			case 2:
				BookingOrder bookingOrder = BengkelService.createBookingOrder(customer, listAllItemService);
				if (!bookingOrders.isEmpty() || bookingOrder != null) {
					bookingOrders.add(bookingOrder);
				}
				break;
			case 3:
				if (customer instanceof MemberCustomer) {
					BengkelService.topUpSaldoCoin(customer);
				}
				else {
					System.out.println("Hanya tersedia untuk member");
				}
				break;
			case 4:
				if (!bookingOrders.isEmpty()) {
					PrintService.printBookingInformation(bookingOrders);
				}
				else {
					System.out.println("Data tidak tersedia.");
				}
				break;
			case 0:
				System.out.println("Logout");
				customer = null;
				isLooping = false;
				break;
			}
		} while (isLooping);
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
