package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	
	public static void printCustomerInformation(Customer customer) {
		String customerStatus = "Non Member";
		double saldoCoin = -1;
		if (customer instanceof MemberCustomer) {
			customerStatus = "Member";
			saldoCoin = ((MemberCustomer) customer).getSaldoCoin();
		}
		System.out.println(customerStatus);
		System.out.printf("%50s\n", "Customer Profile");
		System.out.printf("%-16s: %s\n", "Customer Id", customer.getCustomerId());
		System.out.printf("%-16s: %s\n", "Nama", customer.getName());
		System.out.printf("%-16s: %s\n", "Alamat", customer.getAddress());
		System.out.printf("%-16s: %s\n","Customer Status", customerStatus);
		if (customer instanceof MemberCustomer) {
			System.out.printf("%-16s: Rp. %s\n","Saldo Coin", saldoCoin);
		}
		printVechicle(customer.getVehicles());
	}

	public static void printItemService(List<ItemService> itemServices) {
		String formatTable = "| %-2s | %-15s | %-20s | %-18s | %-18s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Service Id", "Nama", "Tipe Kendaraan", "Harga");
	    System.out.format(line);
		int number = 1;
	    for (ItemService item : itemServices) {
	    	System.out.format(formatTable, number++, item.getServiceId(), item.getServiceName(), item.getVehicleType(), item.getPrice());
	    }
	    System.out.printf(line);
	}

	public static void printBookingInformation(List<BookingOrder> orders) {
		String formatTable = "| %-2s | %-20s | %-20s | %-18s | %-18s | %-30s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Booking Id", "Nama Customer", "Total Services Price", "Total Payment", "List Services");
	    System.out.format(line);
	    int number = 1;
	    for (BookingOrder order : orders) {
	    	System.out.format(formatTable, number, order.getBookingId(), order.getCustomer().getName(), order.getTotalServicePrice(), order.getTotalPayment(), getServices(order.getServices()));
	    	number++;
	    }
	    System.out.printf(line);
	}

	public static String getServices(List<ItemService> itemServices) {
		StringBuilder sb = new StringBuilder();
		for (ItemService s: itemServices) {
			sb.append(String.format("%s, ", s.getServiceName()));
		}
		return sb.toString();
	}
}
