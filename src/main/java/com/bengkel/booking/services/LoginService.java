package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.Customer;

public class LoginService {
    
    public static Customer login(String customerId, String password, List<Customer> customerList) {
        Customer customer = getCustomerById(customerId, customerList);
        if (customer != null) {
            if (isPasswordValid(password, customer)) {
                System.out.println("Login Berhasil!");
            }
            else {
                System.out.println("Password yang anda masukkan salah!");
                customer = null;
            }
        }
        else {
            System.out.println("Customer Id tidak ditemukan!");
        }
        return customer;
    }

    public static Customer getCustomerById(String customerId, List<Customer> customerList) {
        return customerList.stream()
            .filter(customer -> customer.getCustomerId().equals(customerId))
            .findFirst().orElse(null);
    }

    public static boolean isPasswordValid(String password, Customer customer) {
        return customer.getPassword().equals(password);
    }
}
