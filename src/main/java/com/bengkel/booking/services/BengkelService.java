package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class BengkelService {
	
	private static final Scanner input = new Scanner(System.in);
	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	
	//Login
	public static Customer login(List<Customer> customerList) {
		int loginFailedCounter = 0;
		System.out.println("Login");
		System.out.println("Input Exit Untuk batal login");
		do {
			System.out.print("Masukkan Customer-ID: ");
			String customerId = input.nextLine();
			System.out.print("Masukkan Password: ");
			String password = input.nextLine();
			Customer customer = LoginService.login(customerId, password, customerList);
			if (customer != null) {
				return customer;
			}
			else {
				loginFailedCounter++;
			}
		} while(loginFailedCounter != 3);
		if (loginFailedCounter == 3) {
			System.out.println("Login Gagal: Percobaan login gagal 3 kali.");
			System.exit(-1);
		}
		return null;
	}

	//Info Customer
	public static void showCustomerInfo(Customer customer) {
		PrintService.printCustomerInformation(customer);
	}

	//Booking atau Reservation
	public static BookingOrder createBookingOrder(Customer customer, List<ItemService> itemServices) {
		System.out.println("Buat Booking Baru");
		System.out.println("Inputkan exit untuk batal / keluar.");
		boolean isDone = false;
		BookingOrder order = null;
		order = new BookingOrder(customer);
		do {
			System.out.print("Masukkan Vehicle ID: ");
			String vehicleId = input.nextLine();
			Vehicle vehicle = getVehicleById(vehicleId, customer.getVehicles());
			if (vehicle != null) {
				List<ItemService> itemServiceByVehicleType = getItemServicesByVechileType(itemServices, vehicle.getVehicleType());
				List<ItemService> customerItemChoices = selectItemServices(itemServiceByVehicleType, customer);
				if (customerItemChoices != null) {
					order.setServices(customerItemChoices);
					order.setTotalServicePrice(calculateTotalServicesPrice(customerItemChoices));
					processPayment(customer, order);
					if (order.getStatus() == BookingOrder.ORDER_PAYMENT_CANCELED) {
						order = null;
					}
					isDone = true;
				}
				else {
					order = null;
					isDone = true;
				}	
			}
			else {
				System.out.println("Vehicle ID tidak ditemukan");
			}
		} while(!isDone);
		return order;
	}

	public static void processPayment(Customer customer, BookingOrder order) {
		String paymentMethod = "Cash";
		if (customer instanceof MemberCustomer) {
			System.out.println("Pilih Metode Pembayaran");
			System.out.println("1. Saldo Coin");
			System.out.println("2. Cash");
			int choice = Validation.validasiNumberWithRange("Choice: ", "Input hanya angka", "[0-9]", 2, 1);
			if (choice == 1) {
				paymentMethod = "Saldo Coin";
			}
		}
		if (paymentMethod.equals("Saldo Coin")) {
			double estimationTotalPrice = order.getTotalServicePrice() - (order.getTotalServicePrice() * 0.1);
			if (!isSaldoCoinSufficient((MemberCustomer)customer, estimationTotalPrice)) {
				onPaymentInsufficient(customer, order);
			}
		}
		System.out.println("Your payment method: " + paymentMethod);
		confirmPayment(order, customer, paymentMethod);
	}

	public static void onPaymentInsufficient(Customer customer, BookingOrder order) {
		System.out.println("Saldo coin anda tidak mencukupi");
		System.out.println("Apakah anda ingin: ");
		System.out.println("1. Top Up Saldo Coin");
		System.out.println("2. Cash");
		System.out.println("0. Batalkan Transaksi");
		int choice = Validation.validasiNumberWithRange("choice: ", "Input hanya angka", "[0-9]", 2, 0);
		switch (choice) {
			case 1:
				topUpSaldoCoin(customer);
				confirmPayment(order, customer, "Saldo Coin");
				break;
			case 2:
				confirmPayment(order, customer, "Cash");
				break;
			case 0:
				order.setStatus(BookingOrder.ORDER_PAYMENT_CANCELED);
				return;
		}
	}

	public static void finalizedPayment(Customer customer, BookingOrder order, String paymentMethod) {
		order.setPaymentMethod(paymentMethod);
		order.calculatePayment();
		order.setStatus(BookingOrder.ORDER_PAYMENT_FINALIZED);
		System.out.println("Pembayaran Berhasil!");
		if (customer instanceof MemberCustomer) {
			MemberCustomer member = (MemberCustomer) customer;
			member.setSaldoCoin(member.getSaldoCoin() - order.getTotalPayment());
		}
	}

	public static void confirmPayment(BookingOrder order, Customer customer, String paymentMethod) {
		System.out.printf("Konfirmasi pembayaran dengan %s\n", paymentMethod);
		System.out.printf("Apakah anda ingin menyelesaikan pembayaran dengan metode %s? Y/T\n", paymentMethod);
		char choice = Validation.validasiInput("choice: ", "Input tidak sesuai", "[tTyY]").charAt(0);
		if (choice == 'y' || choice == 'Y') {
			finalizedPayment(customer, order, paymentMethod);
		}
		else {
			System.out.println("Order dibatalkan.");
			order.setStatus(BookingOrder.ORDER_PAYMENT_CANCELED);
		}
	}

	//Top Up Saldo Coin Untuk Member Customer
	public static void topUpSaldoCoin(Customer customer) {
		System.out.println("Top Up Saldo Coin");
		System.out.println("Minimal Top Up Rp. 100,000,00");
		double nominal = Validation.validasiNumberWithRange("Masukkan nominal: : ", "Input hanya boleh angka dan min", "[0-9]+", 999999999, 100000);
		MemberCustomer member = (MemberCustomer) customer;
		double before = member.getSaldoCoin();
		member.setSaldoCoin(before + nominal);
		System.out.println("Saldo updated");
		System.out.println("Before: Rp. " + before);
		System.out.println("After: Rp. " + member.getSaldoCoin());
	}
	//Logout
	public static void logout() {

	}

	public static Vehicle getVehicleById(String vehicleId, List<Vehicle> vehicles) {
		return vehicles.stream()
			.filter(v -> v.getVehiclesId().equalsIgnoreCase(vehicleId))
			.findFirst().orElse(null);
	}
	
	public static List<ItemService> selectItemServices(List<ItemService> itemServices, Customer customer) {
		int insertCounter = 0;
		System.out.println("Pilih Service");
		System.out.println("Perhatian!!!\nNon Member hanya menambahkan satu Item Services dan Member dapat menambahkan dua Item Services");
		List<ItemService> customerItemChoices = new ArrayList<>();
		do {
			PrintService.printItemService(itemServices);
			System.out.println("Masukkan Service ID: ");
			String serviceId = input.nextLine();
			ItemService itemService = getServiceById(serviceId, itemServices);
			if (itemService != null) {
				if (customerItemChoices.isEmpty() || !customerItemChoices.contains(itemService)) {
					System.out.println("Item berhasil ditambahkan");
					customerItemChoices.add(itemService);
					insertCounter++;
				}
				else {
					System.out.println("Item yang sama sudah ditambahkan.");
				}
				PrintService.printItemService(customerItemChoices);
				if (customer instanceof MemberCustomer && insertCounter != 2) {
					System.out.println("Apakah anda ingin menambahkan lagi Item? Y/T");
					char choice = Validation.validasiInput("choice: ", "Input tidak sesuai", "[tTyY]").charAt(0);
					System.out.println("Anda memilih" + choice);
					if (choice == 't' || choice == 'T') {
						System.out.println("Memilih tidak");
						break;
					}
				}
				else {
					System.out.println("Non Member: Anda telah mencapai limit total Item Services. (1)");
					break;
				}
			}
			else {
				System.out.println("Item Service tidak ditemukan");
				continue;
			}
		} while(insertCounter != 2);
		if (insertCounter == 2) {
			System.out.println("Member: Anda telah mencapai limit total Item Services. (2)");
		}
		return customerItemChoices;
	}

	public static ItemService getServiceById(String id, List<ItemService> itemServices) {
		return itemServices.stream()
			.filter(it -> it.getServiceId().equalsIgnoreCase(id))
			.findFirst().orElse(null);
	}

	public static List<ItemService> getItemServicesByVechileType(List<ItemService> itemServices, String vehicleType) {
		return itemServices.stream()
			.filter(it -> it.getVehicleType().equals(vehicleType))
			.collect(Collectors.toList());
	}

	public static boolean isSaldoCoinSufficient(MemberCustomer customer, double totalPayment) {
		return customer.getSaldoCoin() > totalPayment;
	}

	public static double calculateTotalServicesPrice(List<ItemService> itemServices) {
		return itemServices.stream()
			.mapToDouble(it -> it.getPrice())
			.sum();
	}
}
