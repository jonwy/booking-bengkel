package com.bengkel.booking.util;

import javax.management.modelmbean.InvalidTargetObjectTypeException;

public class IDGenerator {
    
    public static String generateId(String prefix, int index) {
        StringBuilder sb = new StringBuilder(prefix + "-");
        int length = Integer.toString(index).length();
        if (length == 1) {
            sb.append("00" + index);
        }
        if (length == 2) {
            sb.append("0" + index);
        }
        else sb.append(index);
        return sb.toString();
    }
}
